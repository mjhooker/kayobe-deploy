#!/bin/bash

# deploy from https://docs.openstack.org/kayobe/latest/contributor/automated.html

OSREL=stable/train
OSREL=stable/ussuri
echo applying OSREL $OSREL
echo "Step 1"
git clone https://opendev.org/openstack/kayobe.git
cd kayobe
git checkout $OSREL
mkdir -p config/src
git clone https://opendev.org/openstack/kayobe-config-dev.git config/src/kayobe-config
#git clone https://opendev.org/openstack/kayobe-config.git config/src/kayobe-config
#git clone https://github.com/mjhooker/kayobe-config-dev.git config/src/kayobe-config
pushd config/src/kayobe-config
git checkout $OSREL
popd
echo "Step 2"
ip l add breth1 type bridge
ip l set breth1 up
ip a add 192.168.33.3/24 dev breth1
ip l add eth1 type dummy
ip l set eth1 up
ip l set eth1 master breth1
echo "Step 3"
./dev/install-dev.sh
echo "Step 4"
./dev/overcloud-deploy.sh
